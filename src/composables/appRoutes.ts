export const routes = [
  {
    title: 'Heparin',
    path: '/heparin'
  },
  {
    title: 'Antibiotics',
    path:'/atb'
  },
  {
    title: 'Contacts',
    path:'/contact'
  },
  {
    title: 'Nutrition',
    path: '/nutrition'
  },
]

